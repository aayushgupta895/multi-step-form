let currentForm = 1;
let info = {};
let planPrice = [9, 12, 15];
let addonPrices = [1, 2, 2];
let multiplayer = 1;
let mode = "mo";

document.querySelector(".goBack").style.visibility = "hidden";

const btn = document.querySelector(".nextStep");
btn.addEventListener("click", function (event) {
  console.log("currentform ", currentForm);
  event.preventDefault();

  if (currentForm == 4) {
    document.querySelector(".forms").classList.add("dn");
    document.querySelector(".thankYou").classList.remove("dn");
  }

  if (currentForm == 3) {
    currentForm = 4;
    console.log(info);
    info["addon"] = {};
    document.querySelectorAll(".pick-addon").forEach((node, index, arr) => {
      const check = node.querySelector("input");
      if (check.classList.contains("clicked")) {
        const addonTitle = node.querySelector(".addOns-title");
        info["addon"][addonTitle.textContent] =
          addonPrices[index] * multiplayer;
      }
    });
    console.log(info);
    document.querySelector(".fm-addon").style.display = "none";
    changeTitle(currentForm);
    document.querySelector(".selected-title").textContent = info["plan"];
    document.querySelector(
      ".selected-price"
    ).textContent = `$${info["plan-price"]}/${mode}`;
    const services = document.querySelector(".service-select");
    let str = "";
    Object.keys(info["addon"]).forEach((key, ind) => {
      str += `<div class="s${ind}">
                <p>${[key]}</p>
                <p>${info["addon"][key]}/${mode}</p>
              </div>`;
    });
    services.innerHTML = str;
    getTotalPrice();
    document.querySelector(
      ".total-price"
    ).textContent = `$${info["total-price"]}/${mode}`;
    document.querySelector(".totalPerYOM").textContent =
      mode == "mo" ? `Total (per month)` : `Total (per year)`;
    document.querySelector(".selected-title").textContent =
      mode == "mo" ? `${info["plan"]} (Monthly)` : `${info["plan"]} (Yearly)`;
    document.querySelector(".fm-checkout").style.display = "flex";
    changeSerialStyle(2, 3);
  }

  if (currentForm == 2) {
    console.log(`here`);
    let ischecked = false;
    document.querySelectorAll(".options").forEach((node, index) => {
      if (node.classList.contains("clicked")) {
        ischecked = true;
        info["plan"] = node.querySelector(".opt-title").textContent;
        info["plan-price"] = planPrice[index] * multiplayer;
        console.log(info, "inside 2");
      }
    });
    if (ischecked) {
      currentForm = 3;
      console.log(currentForm);
      changeTitle(currentForm);
      document.querySelector(".fm-plan").style.display = "none";

      let str = "";
      document.querySelectorAll(".addOns-price").forEach((node, ind) => {
        node.textContent = `+${addonPrices[ind] * multiplayer}/${mode}`;
      });
      document.querySelector(".fm-addon").style.display = "flex";
      changeSerialStyle(1, 2);
    }
  }

  if (currentForm == 1) {
    let isValid = true;
    const name = document.querySelector("#name").value.trim();
    const email = document.querySelector("#email").value.trim();
    const phone = document.querySelector("#phone").value.trim();
    console.log(name, email, phone);

    // Name validation
    if (name === "" || name.length < 2) {
      document.querySelector(".n-warn").style.display = "inline";
      document.querySelector("#name").style.border =
        "2px solid var(--strawberry-red)";
      isValid = false;
    }

    const emailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailReg.test(email)) {
      document.querySelector(".e-warn").style.display = "inline";
      document.querySelector("#email").style.border =
        "2px solid var(--strawberry-red)";
      isValid = false;
    }

    const phoneReg = /^\+\d{2}\s?\d{10}$/;

    if (!phoneReg.test(phone)) {
      document.querySelector(".p-warn").style.display = "inline";
      document.querySelector("#phone").style.border =
        "2px solid var(--strawberry-red)";
      isValid = false;
    }

    if (isValid) {
      info["phone"] = phone;
      info["name"] = name;
      info["email"] = email;
      currentForm = 2;
      document.querySelector(".n-warn").style.display = "none";
      document.querySelector("#name").style.border = `var(--cool-gray)`;
      document.querySelector("#email").style.border = `var(--cool-gray)`;
      document.querySelector("#phone").style.border = `var(--cool-gray)`;
      document.querySelector(".e-warn").style.display = "none";
      document.querySelector(".p-warn").style.display = "none";
      changeTitle(currentForm);

      const serial = document.querySelectorAll(".serial");
      console.log(serial, "serail");
      changeSerialStyle(0, 1);
      document.querySelector(".fm-pinfo").style.display = "none";
      document.querySelector(".fm-plan").style.display = "flex";
      document.querySelector(".goBack").style.visibility = "visible";
    }
  }
});

const toggle = document.querySelector("#checkbox");
toggle.addEventListener("click", function () {
  if (this.checked) {
    multiplayer = 10;
    mode = "yr";
    document.querySelector(".toggler").style.marginTop = "2.5rem";
    document.querySelectorAll(".opt-price").forEach((element, index) => {
      element.textContent = `${planPrice[index] * multiplayer}/${mode}`;
    });
    document.querySelectorAll(".opt-free").forEach((element) => {
      element.style.display = "inline";
    });
  } else {
    multiplayer = 1;
    mode = "mo";
    document.querySelectorAll(".opt-price").forEach((element, index) => {
      element.textContent = `${planPrice[index]}/${mode}`;
    });
    document.querySelector(".toggler").style.marginTop = "3.5rem";
    document.querySelectorAll(".opt-free").forEach((element) => {
      element.style.display = "none";
    });
  }
});

document.querySelectorAll(".options").forEach((node, index, arr) => {
  node.addEventListener("click", () => {
    for (let element of arr) {
      element.classList.remove("clicked");
      element.style.borderColor = "var(--light-gray)";
      element.style.backgroundColor = "white";
    }
    console.log(node);
    node.classList.add("clicked");
    node.style.borderColor = "var(--purplish-blue)";
    node.style.backgroundColor = "var(--alabaster)";
  });
});

document.querySelectorAll(".pick-addon").forEach((node, index, arr) => {
  const check = node.querySelector("input");
  check.addEventListener("click", function () {
    if (this.checked) {
      check.classList.add("clicked");
      node.style.borderColor = "var(--purplish-blue)";
    } else {
      check.classList.remove("clicked");
      node.style.borderColor = "var(--light-gray)";
    }
  });
});

document.querySelector(".goBack").addEventListener("click", function () {
  if (currentForm == 4) {
    currentForm -= 1;
    changeSerialStyle(3, 2);
    changeTitle(currentForm);
    document.querySelector(".fm-checkout").style.display = "none";
    document.querySelector(".fm-addon").style.display = "flex";
  } else if (currentForm == 3) {
    currentForm -= 1;
    changeSerialStyle(2, 1);
    changeTitle(currentForm);
    document.querySelector(".fm-addon").style.display = "none";
    document.querySelector(".fm-plan").style.display = "flex";
  } else if (currentForm == 2) {
    currentForm -= 1;
    changeSerialStyle(1, 0);
    changeTitle(currentForm);

    document.querySelector(".goBack").style.visibility = "hidden";
    document.querySelector(".fm-pinfo").style.display = "flex";
    document.querySelector(".fm-plan").style.display = "none";
  }
});

document.querySelector(".change-btn").addEventListener("click", function () {
  changeSerialStyle(3, 1);
  currentForm = 2;
  changeBtnColor(currentForm);
  document.querySelector(".nextStep").textContent = `Next Step`;
  document.querySelector(".fm-plan").style.display = "flex";
  document.querySelector(".fm-checkout").style.display = "none";
});

function changeSerialStyle(ind1, ind2) {
  const serial = document.querySelectorAll(".serial");
  serial[ind2].style.backgroundColor = "var(--pastel-blue)";
  serial[ind2].style.color = `var(--marine-blue)`;
  serial[ind2].style.border = ` 1px solid var(--pastel-blue)`;
  serial[ind1].style.backgroundColor = `transparent`;
  serial[ind1].style.color = `white`;
  serial[ind1].style.border = " 1px solid white";
}

function changeTitle(currentForm) {
  if (currentForm == 4) {
    changeBtnColor(currentForm);
    document.querySelector(".title").textContent = "Finishing up";
    document.querySelector(".title-des").textContent =
      "Double-check everything looks OK before confirming.";
    document.querySelector(".nextStep").textContent = "Confirm";
    document.querySelector(".nextStep").style.backgroundColor =
      "hsl(243, 100%, 62%)";
  } else if (currentForm == 3) {
    changeBtnColor(currentForm);
    document.querySelector(".nextStep").style.backgroundColor =
      "var(--marine-blue)";
    document.querySelector(".nextStep").textContent = "Next Step";

    document.querySelector(".title").textContent = "Pick add-ons";
    document.querySelector(".title-des").textContent =
      "Add-ons helps enhance your gaming experience";
  } else if (currentForm == 2) {
    document.querySelector(".title").textContent = "Select your plan";
    document.querySelector(".title-des").textContent =
      "You have the option of monthly or yearly billing";
  } else if (currentForm == 1) {
    document.querySelector(".title").textContent = "Personal info";
    document.querySelector(".title-des").textContent =
      "Please provide your name, email address, and phone number.";
  }
}

function changeBtnColor(currentForm) {
  colorOnMouseOver = "hsl(213, 96%, 18%)";
  colorOnMouseOut = "hsl(213, 96%, 18%)";
  if (currentForm == 4) {
    colorOnMouseOver = "#473dffbf";
    colorOnMouseOut = "#473dff";
  }
  document.querySelector(".nextStep").style.backgroundColor = colorOnMouseOut;
  document
    .querySelector(".nextStep")
    .addEventListener("mouseover", function () {
      this.style.backgroundColor = colorOnMouseOver;
    });
  document.querySelector(".nextStep").addEventListener("mouseout", function () {
    this.style.backgroundColor = colorOnMouseOut;
  });
}

function getTotalPrice() {
  let totalPrice = info["plan-price"];
  Object.values(info["addon"]).forEach((value) => {
    totalPrice += +value;
  });
  info["total-price"] = totalPrice;
}
